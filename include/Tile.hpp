#ifndef INCLUDE_TILE_HPP
#define INCLUDE_TILE_HPP

#include "ResourceManager.hpp"
#include "Logger.hpp"
#include <SFML/Graphics.hpp>

namespace jumprun
{
	class Tile
	{
		sf::Sprite sprite;
		const sf::Vector2f size{ 32, 32 };
		sf::Vector2f spritePosition;
		sf::Vector2f position;
		std::shared_ptr<sf::Texture> texture;
		
		bool solid{true};

		TextureManager& textureManager;
		Logger& logger;
	public:
		Tile(std::string, sf::Vector2f, TextureManager& const, bool, Logger&);

		void draw(sf::RenderTarget& const);
		sf::Vector2f getPosition() const;
		bool isSolid() const;
	};

	inline bool Tile::isSolid() const { return solid; }
	inline sf::Vector2f Tile::getPosition() const { return position; }
}

#endif // !INCLUDE_TILE_HPP
