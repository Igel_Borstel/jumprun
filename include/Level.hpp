#ifndef INCLUDE_LEVEL_HPP
#define INCLUDE_LEVEL_HPP

#include <memory>
#include <vector>
#include "ResourceManager.hpp"
#include "Logger.hpp"
#include "Tile.hpp"

namespace jumprun
{
	class Tile;

	using TilePtr = std::shared_ptr<Tile>;
	using TileMap = std::vector<std::vector<TilePtr>>;

	class Level
	{
		TileMap backgroundTiles;
		std::size_t width;
		std::size_t height;
		Logger& logger;
		TextureManager& textureManager;
		sf::RenderTexture tileTexture;
		
		bool tileChanged{ true };
	public:
		Level(TextureManager& const tm, Logger&);
		TilePtr getTile(std::size_t, std::size_t);
		int getWidth() const;
		int getHeight() const;
		void draw(sf::RenderTarget&);
	};

	inline TilePtr Level::getTile(std::size_t x, std::size_t y) { return backgroundTiles[x][y]; }
	inline int Level::getWidth() const { return width; }
	inline int Level::getHeight() const { return height; }
}

#endif // !INCLUDE_LEVEL_HPP
