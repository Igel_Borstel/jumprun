#ifndef INCLUDE_SPRITESHEET_HPP
#define INCLUDE_SPRITESHEET_HPP

#include "ResourceManager.hpp"
#include <SFML/System.hpp>

namespace jumprun
{
	void loadTextureFromSpriteSheet(std::string, sf::IntRect, std::string, TextureManager& const);
}

#endif // !INCLUDE_SPRITESHEET_HPP
