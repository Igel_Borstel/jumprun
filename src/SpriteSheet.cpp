#include "../include/SpriteSheet.hpp"
#include <memory>
#include <utility>

namespace jumprun
{
	void loadTextureFromSpriteSheet(std::string path, sf::IntRect rect, std::string name, TextureManager& const textureManager)
	{
		auto texture = std::make_shared<sf::Texture>();
		if (!texture->loadFromFile(path, rect))
		{
			std::stringstream errmsg;
			errmsg << "Error during resource loading! File does not exists or could not opened. File: " << path;
			throw std::runtime_error(errmsg.str());
		}
		textureManager.addResource(name, std::move(texture));
	}
}