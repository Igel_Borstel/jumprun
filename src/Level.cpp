#include "../include/Level.hpp"
#include <SFML/Graphics.hpp>
#include <memory>

namespace jumprun
{
	Level::Level(TextureManager& const tm, Logger& log)
		:logger{ log }, textureManager{ tm }, width{ 1 }, height{ 1 }, tileChanged{ true }
	{
		std::vector<std::vector<int>> tilemap{ {4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
											   {0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
											   {1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 6},
											   {3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
											   {3, 0, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0},
											   {3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0},
											   {3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 3, 1, 0, 0, 0, 0, 0},
											   {3, 3, 7, 3, 2, 2, 1, 0, 0, 1, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
											   {3, 3, 3, 3, 2, 2, 3, 0, 1, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
											   {3, 3, 3, 3, 3, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
											   {3, 3, 3, 3, 3, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
											   {3, 3, 3, 3, 3, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
											   {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5}};
		height = tilemap.size();
		width = tilemap.at(0).size();
		for (int i = 0; i < tilemap.size(); ++i)
		{
			std::vector<TilePtr> row;
			for (int j = 0; j < tilemap[i].size(); ++j)
			{
				TilePtr ptr = nullptr;
				switch (tilemap[i][j])
				{
					case 0:
						ptr = std::make_shared<Tile>("transparent", sf::Vector2f{ static_cast<float>(j), static_cast<float>(i) }, textureManager, false, logger);
						break;
					case 1:
						ptr = std::make_shared<Tile>("gras", sf::Vector2f{ static_cast<float>(j), static_cast<float>(i)}, textureManager, true, logger);
						break;
					case 2:
						ptr = std::make_shared<Tile>("water", sf::Vector2f{ static_cast<float>(j), static_cast<float>(i)}, textureManager, true, logger);
						break;
					case 3:
						ptr = std::make_shared<Tile>("stone", sf::Vector2f{ static_cast<float>(j), static_cast<float>(i)}, textureManager, true, logger);
						break;
					case 4:
						ptr = std::make_shared<Tile>("tlc", sf::Vector2f{ static_cast<float>(j), static_cast<float>(i) }, textureManager, false, logger);
						break;
					case 5:
						ptr = std::make_shared<Tile>("brc", sf::Vector2f{ static_cast<float>(j), static_cast<float>(i) }, textureManager, false, logger);
						break;
					case 6:
						ptr = std::make_shared<Tile>("goal", sf::Vector2f{ static_cast<float>(j), static_cast<float>(i) }, textureManager, false, logger);
						break;
					case 7:
						ptr = std::make_shared<Tile>("trap", sf::Vector2f{ static_cast<float>(j), static_cast<float>(i) }, textureManager, false, logger);
						break;
					default:
						break;
				}
				row.push_back(ptr);
			}
			backgroundTiles.push_back(row);
		}
		tileTexture.create(width * 32, height * 32);
	}

	void Level::draw(sf::RenderTarget& rt)
	{
		if (tileChanged)
		{
			tileTexture.create(width * 32, height * 32);
			for (auto tilerow : backgroundTiles)
			{
				for (auto tile : tilerow)
				{
					tile->draw(tileTexture);
				}
			}
			tileTexture.display();
			tileChanged = true;
		}
		sf::Sprite sprite(tileTexture.getTexture());
		rt.draw(sprite);
	}
}