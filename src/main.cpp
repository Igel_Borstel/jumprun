#include <SFML/Graphics.hpp>
#include "../include/ResourceManager.hpp"
#include "../include/Tile.hpp"
#include "../include/Logger.hpp"
#include "../include/SpriteSheet.hpp"
#include "../include/Level.hpp"
#include <iostream>

int main()
{
	jumprun::Logger logger;
	jumprun::TextureManager textureManager;
	/*
	textureManager.loadResource("gras", "resources/gfx/gras.png");
	textureManager.loadResource("water", "resources/gfx/water.png");
	textureManager.loadResource("stone", "resources/gfx/stone.png");
	textureManager.loadResource("transparent", "resources/gfx/transparent.png");
	*/
	jumprun::loadTextureFromSpriteSheet("resources/gfx/tileset.png", sf::IntRect{ 0, 0, 32, 32 }, "gras", textureManager);
	jumprun::loadTextureFromSpriteSheet("resources/gfx/tileset.png", sf::IntRect{ 32, 0, 32, 32 }, "water", textureManager);
	jumprun::loadTextureFromSpriteSheet("resources/gfx/tileset.png", sf::IntRect{ 64, 0, 32, 32 }, "stone", textureManager);
	jumprun::loadTextureFromSpriteSheet("resources/gfx/tileset.png", sf::IntRect{ 96, 0, 32, 32 }, "transparent", textureManager);
	jumprun::loadTextureFromSpriteSheet("resources/gfx/tileset.png", sf::IntRect{ 128, 0, 32, 32 }, "tlc", textureManager);
	jumprun::loadTextureFromSpriteSheet("resources/gfx/tileset.png", sf::IntRect{ 160, 0, 32, 32 }, "brc", textureManager);
	jumprun::loadTextureFromSpriteSheet("resources/gfx/tileset.png", sf::IntRect{ 192, 0, 32, 32 }, "goal", textureManager);
	jumprun::loadTextureFromSpriteSheet("resources/gfx/tileset.png", sf::IntRect{ 224, 0, 32, 32 }, "trap", textureManager);
	jumprun::Level level(textureManager, logger);
	/*std::vector<int> tilemap{ 0, 0, 0, 0, 0, 0, 0,
							  1, 1, 1, 0, 0, 0, 0, 
							  3, 3, 3, 1, 0, 0, 0,
							  3, 3, 3, 1, 2, 2, 2,
							  3, 0, 3, 1, 2, 2, 2,
							  3, 3, 3, 3, 1, 2, 2,
							  3, 3, 3, 3, 3, 1, 2,
							  3, 3, 3, 3, 3, 3, 1,
							  3, 3, 3, 3, 3, 3, 3};
	int horizontalTiles = 7;
	int verticalTiles = 7;
	int width = 32;
	int height = 32;

	std::vector<jumprun::Tile> tiles;
	int row = 0;
	int cnt = 0;
	for (unsigned int i = 0; i < tilemap.size(); ++i, ++cnt)
	{
		if (cnt >= horizontalTiles)
		{
			++row;
			cnt = 0;
		}
		sf::Vector2i tilePos;
		tilePos.x = i % horizontalTiles;
		tilePos.y = row;
		sf::Vector2f pos;
		pos.x = tilePos.x;// * 32.f + 16.f;
		pos.y = tilePos.y;// * 32.f + 16.f;
		std::stringstream stream;
		stream << "Tile " << i << " (" << tilePos.x << "|" << tilePos.y << ") [" << pos.x << "|" << pos.y << "] Type: " << tilemap.at(i) << std::endl;
		logger.fine(stream.str());
		if (tilemap.at(i) == 0)
		{
			tiles.push_back(jumprun::Tile{ "transparent", pos, textureManager, true, logger});
		}
		else if (tilemap.at(i) == 1)
		{
			tiles.push_back(jumprun::Tile{ "gras", pos, textureManager, true, logger});
		}
		else if (tilemap.at(i) == 2)
		{
			tiles.push_back(jumprun::Tile{ "water", pos, textureManager, true, logger});
		}
		else if (tilemap.at(i) == 3)
		{
			tiles.push_back(jumprun::Tile{ "stone", pos, textureManager, true, logger});
		}
	}
	*/
	sf::View view(sf::FloatRect(0, 0, 672, 416));
	sf::RenderWindow window(sf::VideoMode(672, 416), "SFML works!");
	
	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window.close();
			if (event.type == sf::Event::KeyReleased && event.key.code == sf::Keyboard::Down)
				view.move(0.f, 32.f);
			if (event.type == sf::Event::KeyReleased && event.key.code == sf::Keyboard::Up)
				view.move(0.f, -32.f);
			if (event.type == sf::Event::KeyReleased && event.key.code == sf::Keyboard::F11)
				view.zoom(1.5);
			if (event.type == sf::Event::KeyReleased && event.key.code == sf::Keyboard::F10)
				view.zoom(0.5);
		}
		window.setView(view);
		window.clear(sf::Color::White);
		level.draw(window);
		window.display();
	}

	return 0;
}