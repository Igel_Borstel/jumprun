#include "../include/Tile.hpp"

namespace jumprun
{
	Tile::Tile(std::string name, sf::Vector2f pos, TextureManager& const tm, bool sol, Logger& log)
		: textureManager{ tm }, position{ pos }, solid{ sol }, logger{ log }
	{
		spritePosition.x = position.x * size.x + 0.5f * size.x;
		spritePosition.y = position.y * size.y + 0.5f * size.y;
		texture = tm.getResource(name);
		if (texture == nullptr)
		{
			texture = std::make_shared<sf::Texture>();
			logger.error("Error during textureloading! Texture " + name + " does not exist!\n");
			if (!texture->create(static_cast<int>(size.x), static_cast<int>(size.y)))
			{
				throw std::runtime_error("Could not create empty Texture!");
			}
		}
		sprite.setTexture(*texture, true);
		sprite.setOrigin(texture->getSize().x / 2.f, texture->getSize().y / 2.f);
		sprite.setPosition(spritePosition);
	}

	void Tile::draw(sf::RenderTarget& const rt)
	{
		sprite.setPosition(spritePosition);
		rt.draw(sprite);
	}
}